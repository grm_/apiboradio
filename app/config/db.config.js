module.exports = {
    HOST:'localhost',
    DB:'bosoychile',
    USER:'root',
    PASSWORD:'secret',
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };