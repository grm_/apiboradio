const db = require("../models");
const Video = db.video;
const Op = db.Sequelize.Op;

// Create and Save a new Video
exports.create = (req, res) => {
  
};

// Retrieve all video from the database.
exports.findAll = (req, res) => {
  
};

// Find a single Video with an id
exports.findOne = (req, res) => {
  
};

// Update a Video by the id in the request
exports.update = (req, res) => {
  
};

// Delete a Video with the specified id in the request
exports.delete = (req, res) => {
  
};

// Delete all video from the database.
exports.deleteAll = (req, res) => {
  
};

// Find all published video
exports.findAllPublished = (req, res) => {
  
};