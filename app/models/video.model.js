module.exports = (sequelize, Sequelize) => {
    const Video = sequelize.define("video", {
      _index: {
        type: Sequelize.STRING
      },
      _type: {
        type: Sequelize.STRING
      },
      _id: {
        type: Sequelize.INTEGER
      },
      _version: {
        type: Sequelize.STRING
      },
      found: {
        type: Sequelize.STRING
      },
      _source: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return Video;
  };