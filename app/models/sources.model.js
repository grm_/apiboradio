module.exports = (sequelize, Sequelize) => {
    const Source = sequelize.define("source", {
        title: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      subcategory_slug: {
        type: Sequelize.BOOLEAN
      },
      tags: {
        type: Sequelize.STRING
      },
      found: {
        type: Sequelize.STRING
      },
      _source: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return Source;
  };